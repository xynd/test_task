<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders}}`.
 */
class m190408_102352_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'user' => $this->string(),
            'link' => $this->string(),
            'quantity' => $this->integer(),
            'status' => $this->tinyInteger(),
            'created_at' => $this->integer(),
            'mode' => $this->tinyInteger(),
        ]);
        $this->createIndex(
            'idx-orders-id',
            'orders',
            'id',
            'true');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
        $this->dropIndex(
            'idx-orders-id',
            'orders'
        );
    }
}
