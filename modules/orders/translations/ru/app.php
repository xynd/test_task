<?php
/**
 * Created by PhpStorm.
 * User: xynd
 * Date: 08.04.2019
 * Time: 14:24
 */
return [
    'app'=>'app',
    'All orders'=>'Все заказы',
    'Pending'=>'В ожидании',
    'In progress'=>'В процессе',
    'Completed'=>'Завершенные',
    'Canceled'=>'Отмененные',
    'Error'=>'Ошибка',
    'Id'=>'Номер',
    'User'=>'Пользователь',
    'Link'=>'Ссылка',
    'Quantity'=>'Количество',
    'Service'=>'Сервис',
    'Status'=>'Статус',
    'Mode'=>'Режим',
    'Created'=>'Создан',
    'OrderId'=>'Номер Заказа',
    'Usernames'=>'Имя пользователя',
    'Failed'=>'Неудачно',
    'Auto'=>'Автоматический',
    'Manual'=>'Ручной',
    'Followers'=>'Подписчики',
    'Likes'=>'Лайки',
    'Views'=>'Просмотры',
    'Tweets'=>'Твиты',
    'Retweets'=>'Ретвиты',
    'Comments'=>'Коментарии',
    'Custom comments'=>'Пользовательские коментраии',
    'Page Likes'=>'Лайки страниц',
    'Post Likes'=>'Лайки сообщений',
    'Friends'=>'Друзья',
    'SEO'=>'Поисковая оптимизация',
    'Mentions'=>'Упоминания',
    'Mentions with Hashtags'=>'Упоминания с хэштэгами',
    'Mentions with custom list'=>'Упоминание с списком',
    'Mentions Hashtag'=>'Хэштэги упоминаний',
    'Mentions User Followers'=>'Упоминания подписчиков',
    'Mentions Media Likers'=>'Упоминания в сми'
];