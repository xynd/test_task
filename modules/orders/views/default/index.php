<?php

use app\modules\orders\widgets\multilang;
use yii\helpers\Html;
use app\modules\orders\models\Service;
use kartik\grid\GridView;

?>
<?= multilang\Multilang::widget(['cssClass' => 'pull-right language']); ?>
<div class="container-fluid">
    <ul class="nav nav-tabs p-b">
        <li><?= Html::a(Yii::t('app', 'All orders'), '/orders/?OrderSearch[service_id]=&OrderSearch[status]=&OrderSearch[mode]=') ?></li>
        <li><?= Html::a(Yii::t('app', 'Pending'), '/orders/?OrderSearch[service_id]=&OrderSearch[status]=0&OrderSearch[mode]=') ?></li>
        <li><?= Html::a(Yii::t('app', 'In progress'), '/orders/?OrderSearch[service_id]=&OrderSearch[status]=1&OrderSearch[mode]=') ?></li>
        <li><?= Html::a(Yii::t('app', 'Completed'), '/orders/?OrderSearch[service_id]=&OrderSearch[status]=2&OrderSearch[mode]=') ?></li>
        <li><?= Html::a(Yii::t('app', 'Canceled'), '/orders/?OrderSearch[service_id]=&OrderSearch[status]=3&OrderSearch[mode]=') ?></li>
        <li><?= Html::a(Yii::t('app', 'Error'), '/orders/?OrderSearch[service_id]=&OrderSearch[status]=4&OrderSearch[mode]=') ?></li>
        <li class="pull-right custom-search"><?= $this->render('_search', ['model' => $searchModel]) ?></li>
    </ul>
    <?php
    echo kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => &$searchModel,
        'layout' => '{items}{pager}<span class="col-sm-4 pagination-counters">{summary}{toolbar}</span>',
        'toolbar' => [
            '{export}',
            '{toggleData}',
        ],
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false,
            'target' => GridView::TARGET_BLANK,
        ],
        'exportConfig' => [
            GridView::CSV => [],
        ],
        'summary' => '{begin} to {end} of {totalCount}',
        'headerRowOptions' => ['style' => 'display: none;'],
        'filterRowOptions' => ['style' => 'font-weight: bold'],
        'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
        'columns' => [
            [
                'filter' => Yii::t('app', 'Id'),
                'format' => 'text',
                'attribute' => 'id',
                'content' => function ($model) {
                    return $model->id;

                }
            ],
            [
                'filter' => Yii::t('app', 'User'),
                'attribute' => 'user',
                'format' => 'text'
            ],
            [
                'filter' => Yii::t('app', 'Link'),
                'attribute' => 'link',
                'format' => 'url',
                'content' => function ($model) {
                    return Html::a(
                        substr($model->link, 20),
                        $model->link);

                }
            ],
            [
                'filter' => Yii::t('app', 'Quantity'),
                'attribute' => 'quantity',
                'format' => 'text'
            ],
            [
                'attribute' => 'service_id',
                'format' => 'text',
                'content' => function ($model) use ($count) {
                    return '<span class="label-id">' . $count[$model->service_id] . '</span> ' . Yii::t('app', $model->getServiceName());
                },
                'filter' => Service::getServiceNames(),
                'filterInputOptions' => ['prompt' => Yii::t('app', 'Service'), 'class' => 'form-control', 'service_id' => null,]
            ],

            [
                'attribute' => 'status',
                'format' => 'integer',
                'filter' => Yii::t('app', 'Status'),
                'content' => function ($model) {
                    switch ($model->status) {
                        case 0:
                            return Yii::t('app', 'Pending');
                            break;
                        case 1:
                            return Yii::t('app', 'In progress');
                            break;
                        case 2:
                            return Yii::t('app', 'Completed');
                            break;
                        case 3:
                            return Yii::t('app', 'Canceled');
                            break;
                        case 4:
                            return Yii::t('app', 'Failed');
                            break;
                    }
                },
            ],
            [
                'attribute' => 'mode',
                'format' => 'integer',
                'content' => function ($model) {
                    return ($model->mode == 1) ? Yii::t('app', 'Auto') : Yii::t('app', 'Manual');
                },
                'filter' => ['0' => Yii::t('app', 'Manual'), '1' => Yii::t('app', 'Auto')],
                'filterInputOptions' => ['prompt' => Yii::t('app', 'Mode'), 'class' => 'form-control', 'mode' => null]
            ],
            [
                'filter' => Yii::t('app', 'Created'),
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d H:i:s']
            ]
        ]
    ]); ?>

