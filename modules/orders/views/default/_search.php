<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use models\OrderSearch;

?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-inline'],
    'action' => ['index'],
    'method' => 'get',
]);
$items = [
    0 => Yii::t('app', 'OrderId'),
    1 => Yii::t('app', 'Usernames'),
    2 => Yii::t('app', 'Link'),
];
$params = [
    '0' => ['options' => ['selected' => true]]
];
?>
<div class="input-group">
    <?= $form->field($model, 'myfilter')->label(false) ?>
    <?= $form->field($model, 'check')->dropDownList($items, $params)->label(false); ?>
    <?= Html::submitButton('<span class="glyphicon glyphicon-search" aria-hidden="true"></span>', ['class' => 'btn btn-default']) ?>
</div>
<?php ActiveForm::end(); ?>
