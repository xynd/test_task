<?php
/**
 * Created by PhpStorm.
 * User: xynd
 * Date: 08.04.2019
 * Time: 16:12
 */
namespace app\modules\orders\widgets\multilang;
use yii\bootstrap\Widget;
class Multilang extends Widget
{
    public $cssClass;
    public function init(){}

    public function run() {
        return $this->render('view', [
            'cssClass' => $this->cssClass,
        ]);

    }
}