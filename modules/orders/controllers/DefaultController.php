<?php

namespace app\modules\orders\controllers;
use yii\web\Controller;
use app\modules\orders\models;
use Yii;

/**
 * Default controller for the `orders` module
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        $orderModel = new models\Order();
        $searchModel = new models\OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination = ['pageSize' => 100];
        $count = $orderModel->getEachServiceCount();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'count' => $count,
        ]);
    }
}
