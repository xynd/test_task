<?php

namespace app\modules\orders\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "services".
 *
 * @property int $id
 * @property string $name
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function getOrder()
    {
        return $this->hasMany(Order::class, ['id_service' => 'id']);
    }

    public function getCount()
    {
        $result = Service::find()->select('id')->count();

        return $result;
    }

    /**
     * @return array of service names and count of them
     */
    public function getServiceNames()
    {
        $command = Service::find()->asArray()->all();
        $result = ArrayHelper::map($command, 'id', 'name');
        foreach ($result as $key => &$value) {
            $value = Order::getServiceCount($key) . ' ' . Yii::t('app', $value);
        }
        arsort($result);

        return $result;
    }
}
