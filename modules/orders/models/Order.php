<?php

namespace app\modules\orders\models;
use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $user
 * @property string $link
 * @property int $quantity
 * @property int $service_id
 * @property int $status 0 - Pending, 1 - In progress, 2 - Completed, 3 - Canceled, 4 - Fail
 * @property int $created_at
 * @property int $mode 0 - Manual, 1 - Auto
 */
class Order extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user', 'link', 'quantity', 'service_id', 'status', 'created_at', 'mode'], 'required'],
            [['quantity', 'service_id', 'status', 'created_at', 'mode'], 'integer'],
            [['user', 'link'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'link' => 'Link',
            'quantity' => 'Quantity',
            'service_id' => 'Service ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'mode' => 'Mode',
        ];
    }
    public function getService()
    {
        return $this->hasOne(Service::class, ['id'=>'service_id']);
    }
    public function getServiceName()
    {
        $service = $this->service;
        return $service ? $service->name : '';
    }
    public function getServiceCount($service_id)
    {
        $command = Yii::$app->db->createCommand('SELECT count(id) FROM orders WHERE service_id=:id')
            ->bindParam(':id', $service_id);
        $count= $command->queryOne();
        return $count["count(id)"];

    }
    public function getEachServiceCount()
    {
        $count[0] = Order::find()->count();
        for($i=1;$i<=Service::getCount();$i++){
            $count[$i] = Order::getServiceCount($i);
            }
        return $count;
    }


}
