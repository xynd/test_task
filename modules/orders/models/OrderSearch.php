<?php
/**
 * Created by PhpStorm.
 * User: xynd
 * Date: 04.04.2019
 * Time: 17:44
 */

namespace app\modules\orders\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class OrderSearch extends Order
{
    public $check;
    public $myfilter;

    public function rules()
    {
        return [
            [['service_id', 'status', 'mode', 'check'], 'integer'],
            [['myfilter'], 'safe']
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param  array $params the data array to load, `$_GET`.
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['service_id' => $this->service_id])
            ->andFilterWhere(['status' => $this->status])
            ->andFilterWhere(['mode' => $this->mode]);

        switch ($this->check) {
            case 0:
                $query->orFilterWhere(['id' => $this->myfilter]);
                break;
            case 1:
                $query->orFilterWhere(['LIKE', 'user', $this->myfilter]);
                break;
            case 2:
                $query->orFilterWhere(['LIKE', 'link', $this->myfilter]);
                break;
        }

        return $dataProvider;
    }
}